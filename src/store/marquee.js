import config from '../shared/config'
import Axios from 'axios'
export default {
  list: [],
  defaultList: [
    { marqueeText: '歡迎參觀智慧販賣機1' },
    { marqueeText: '歡迎參觀智慧販賣機2' },
    { marqueeText: '歡迎參觀智慧販賣機3' },
    { marqueeText: '歡迎參觀智慧販賣機4' }
  ],
  customList: config.getCustom().marqueeContent,
  between: 3,
  RSSList: [],
  parseRSS () {
    if (!config.params.customText.RSS_URL.length) return true
    let { RSS_URL } = config.getCustom()
    return Axios.get(RSS_URL)
      .then(res => res.data)
      .then(res => {
        let div = document.createElement('span')
        let list = []
        div.innerHTML = res
        div.querySelectorAll('item')
          .forEach(item => {
            let title = item.querySelector('title').innerText.replace(/<!\[CDATA\[(.*)\]\]>/, '$1')
            list.push(title)
          })
        this.RSSList = list
        return true
      }).catch(error => {
        console.warn(error)
        return false
      })
  },
  getList () {
    let list = config.getCustom().marqueeContent
    let i = 0
    if (this.RSSList.length) {
      this.list = this.RSSList.reduce((prev, curr, index) => {
        let _insert = []
        if (index % this.between === 0) {
          _insert = [list[i++ % list.length], curr]
        } else {
          _insert = [curr]
        }
        return prev.concat(_insert)
      }, [])
    } else {
      this.list = list
    }
    return this.list
  }
}
