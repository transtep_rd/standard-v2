import { clone } from 'underscore'
import i18n from '../../lang/i18n'
export class Product {
  /**
   * @param {{ dm, name, img, s, desc, id, title, price, soldout }} item
   */
  constructor (item) {
    this.dm = item.dm
    this.name = item.name
    this.img = item.img
    this.s = item.s
    this.desc = item.desc
    this.id = item.id
    this.title = item.title
    this.price = item.price
    this.soldout = item.soldout

    // custom object
    this.imgLoadError = false

    this.object = clone(item)
  }

  getObj = () => this.object

  /**
   * 取得根據locale修改的名稱
   * @returns {string}
   * @memberof Product
   */
  getName () {
    let locale = i18n.locale.replace('-', '_')
    let name = `${locale}_name`
    if (name in this.object) {
      return this.object[name]
    } else {
      return this.name
    }
  }
  /**
   * 取得根據locale修改的名稱
   * @returns {string}
   * @memberof Product
   */
  getDesc () {
    let locale = i18n.locale.replace('-', '_')
    let desc = `${locale}_description`
    if (desc in this.object) {
      return this.object[desc]
    } else {
      return this.desc
    }
  }
  /**
   * @param {{ dm, name, img, s, desc, id, title, price, soldout }} item
   */
  setObject (item) {
    this.dm = item.dm
    this.name = item.name
    this.img = item.img
    this.s = item.s
    this.desc = item.desc
    this.id = item.id
    this.title = item.title
    this.price = item.price
    this.soldout = item.soldout

    // custom object
    this.imgLoadError = false

    this.object = clone(item)
  }
}

/**
 * 創建新的商品
 * @param {Object} product
 */
export function createProduct (product) {
  return new Product(product)
}

export default createProduct
