import _ from 'underscore'
import { createProduct, Product } from './product'// eslint-disable-line no-unused-vars

export class Products {
  rowsCount = 3
  windowWidth = 1080

  /**
   * @param {any[]} list
   */
  constructor (list) {
    if (!_.isArray(list)) {
      throw new Error('list is not Array type!')
    }
    this._list = list.map(ele => createProduct(ele))
  }

  get maxItemsPerRow () {
    return Math.ceil(this._list.length / this.rowsCount)
  }

  get rowList () {
    return this.listRendering()
  }

  getList () {
    return this._list
  }

  /**
   * @param {number} id
   */
  getProduct (id) {
    return this._list.find(ele => ele.id === id)
  }

  /**
   * @return {Product[][]}
   */
  listRendering () {
    if (this._list.length < 2) {
      return [
        [
          ...this._list
        ]
      ]
    }
    let rowsItem = this._list.reduce((prev, curr) => {
      if (prev.slice(-1)[0].length === this.maxItemsPerRow) {
        prev.push([curr])
      } else {
        prev.slice(-1)[0].push(curr)
      }
      return prev
    }, ([[]]))

    return rowsItem
  }

  /**
   * @param {Product[]} list
   */
  listOnChange (list) {
    return this._list.some(ele => {
      return !~list.findIndex(_ele => _ele.id === ele.id)
    })
  }

  /**
   * @param {Product[]} products
   */
  changeProducts (products) {
    let listById = _.indexBy(this._list, 'id')
    products.forEach(product => {
      // 只關注price soldout
      if (listById[product.id].getObj().price !== product.price || listById[product.id].soldout !== product.soldout) {
        console.log('item is not same')
        listById[product.id].setObject(createProduct(product))
      }
    })
    return true
  }
}

export function createProducts (list) {
  return new Products(list)
}

export default createProducts
