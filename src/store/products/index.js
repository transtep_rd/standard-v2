import config from '../../shared/config'
import products from './products'

// 整理商品清單，加上type辨別容易開發
class ProductsInterface {
  constructor () {
    this._productsList = products([])
  }
  get list () {
    return this._productsList.getList()
  }

  get Products () {
    return this._productsList
  }

  getProductById (id) {
    return this._productsList.getProduct(id)
  }

  get layerList () {
    return this._productsList.listRendering()
  }

  async renewList () {
    let _list = await config.getStockList()
    if (_list.length !== this._productsList.length) {
      // 全部清單更新
      this._productsList = products(_list)
    } else {
      // 只對商品做更新
      this._productsList.changeProducts(_list)
    }
    return true
  }
}

const productsInterface = new ProductsInterface()

export default productsInterface
