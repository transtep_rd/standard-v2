const paymentMethods = [
  {
    // 名稱
    name: 'easycard',
    // payment/hint帶出的關鍵字
    keywords: ['easycard', 'easycardedc'],
    /** 說明 對應語言包 */
    description: 'EASYCARD_DESC',
    /** 需要觸發器 */
    userTrigger: true,
    /** payment/input需帶出的關鍵字 */
    triggerName: 'easycardedc',
    /** 說明 對應語言包 */
    information: 'EASYCARD_INFO'
  },
  {
    name: 'ipass',
    keywords: ['ipass'],
    description: 'IPASS_DESC',
    userTrigger: true,
    triggerName: 'ipass',
    information: 'IPASS_INFO'
  },
  {
    name: 'phone-pay',
    keywords: ['isc_phone-pay'],
    description: 'PHONE_PAY_DESC',
    userTrigger: false,
    triggerName: false,
    information: 'PHONE_PAY_INFO'
  },
  {
    name: 'trusty',
    keywords: ['isc_trusty'],
    description: 'TRUSTY_DESC',
    userTrigger: false,
    triggerName: false,
    information: 'TRUSTY_INFO'
  },
  {
    name: 'kbzpay',
    keywords: ['psc_'],
    description: 'KBZPAY_DESC',
    userTrigger: false,
    triggerName: false,
    information: 'KBZPAY_INFO'
  }
]

/**
 * 取得支付資料
 * @export
 * @param {string} [paymentName=''] 支付關鍵字
 * @returns {{name, keywords, description, userTrigger, triggerName, information}}
 */
export function getPayment (paymentName = '') {
  if (paymentName === '') return false
  if (paymentMethods.some(ele => ele.keywords.some(key => paymentName.includes(key)))) {
    return paymentMethods.find(ele => ele.keywords.some(key => paymentName.includes(key)))
  }
  return false
}
