import webstomp from 'webstomp-client'
import Vue from 'vue'
import config from './config'

const _ebus = new Vue()
/** @type {webstomp.Client} */
let stomp
export default {
  async connect () {
    stomp = webstomp.client(`ws://${config.params.ebus_ip}:61614/stomp`, { debug: false })
    return new Promise((resolve, reject) => {
      stomp.connect('user', 'password', () => {
        _ebus.$emit('ready')
        console.log('connected')
        config.params.tran_chan.forEach(ele => {
          stomp.subscribe(ele, msg => {
            if (msg.body === '') return
            try {
              let _arg = JSON.parse(msg.body)
              _ebus.$emit(_arg.e, _arg)
              console.log(_arg)
            } catch (error) {
              console.log(error)
            }
          })
        })
        setInterval(() => {
          stomp.send('/topic/heartbeat', '{}')
        }, 20 * 1000)
        resolve()
      })
    })
  },
  on: (events, cb) => _ebus.$on(events, cb),
  once: (events, cb) => _ebus.$once(events, cb),
  emit: (events, ...args) => _ebus.$emit(events, ...args),
  send (data) {
    data.src = 'gui'
    _ebus.$emit('send', data)
    data = typeof data === 'object' ? JSON.stringify(data) : data
    config.params.trig_chan.forEach(ele => {
      if (stomp) {
        stomp.send(ele, data)
      }
    })
    return true
  },
  off: (events, cb) => _ebus.$off(events, cb),
  disconnect: () => {
    stomp.disconnect()
  }
}
