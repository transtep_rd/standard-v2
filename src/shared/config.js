import merge from 'merge'
import axios from 'axios'
import Vue from 'vue'

const wrap = async (fn) => {
  try {
    let result = await fn()
    return result
  } catch (error) {
    console.log(error)
    return false
  }
}

let host = location.hostname
// host = '192.168.170.4'
let params = {
  configFile: 'config.js',
  adList: [],
  channelList: [],
  ebus_ip: host,
  stockList: [],
  tran_chan: ['/topic/app'],
  trig_chan: ['/queue/app'],
  adListUrl: `http://${host}/app/rest/media.cgi`,
  channelListUrl: `http://${host}/app/rest/channel.cgi`,
  stockListUrl: `http://${host}/app/rest/stock.cgi`,
  sessData: `http://${host}/app/rest/sess.cgi`,
  debug: true,
  lang: localStorage.getItem('localization') || 'zh-tw',
  customText: {
    marqueeContent: [
      '歡迎參觀智慧販賣機1',
      '歡迎參觀智慧販賣機2',
      '歡迎參觀智慧販賣機3'
    ],
    'kick-title': '歡迎參觀智慧販賣機',
    RSS_URL: 'https://tw.appledaily.com/rss/newcreate/kind/rnews/type/new'
  }
}

/**
 * @typedef Custom
 * @type {{'kick-title':string, marqueeContent:string[], RSS_URL:string}}
 */

/**
 * @param {string} url get url
 * @returns {Promise<Custom>}
 */
function getData (url) {
  return new Promise((resolve, reject) => {
    axios.get(url)
      .then(res => resolve(res.data))
      .catch(err => resolve(err))
  })
}

export default {
  params,
  async configure () {
    let _config = await fetch(this.params.configFile)
      .then(res => res.data)
      .catch(err => {
        console.warn(err)
        return false
      })
    // config檔案不應該有customText
    if (_config && 'customText' in _config) {
      delete _config.customText
    }
    // 設定檔整合
    if (_config) {
      this.params = merge.recursive(true, params, _config)
      console.log(params)
    }
    // 取得本地端文字檔設定
    let localSetup = await getData(`http://${this.params.ebus_ip}/app/rest/set_text.php`)
    // 取得LTMS文字檔設定
    let ltmsSetup = await getData(`http://${this.params.ebus_ip}/media/ivm-text.json`)
    this.params.customText = merge.recursive(true, this.params.customText, localSetup, ltmsSetup)
    return true
  },
  /**
   * @desc 取得stock清單
   * @returns {Promise<{dm, name, img, s, desc, id, title, price, soldout}[]>}
   */
  async getStockList () {
    let self = this
    this.params.stockList = await wrap(() => fetch(self.params.stockListUrl + '?' + new Date().valueOf()).then(res => res.json()))
    return this.params.stockList
  },
  /**
   * @desc 取得多媒體播放清單
   * @returns {Promise<{src, desc, position, type, title, id, duration?}[]>}
   */
  async getAdList () {
    let self = this
    this.params.adList = await wrap(() => fetch(self.params.adListUrl + '?' + new Date().valueOf()).then(res => res.json()))
    return this.params.adList
  },
  /**
   * @desc 取得存量清單`貨道單位`
   * @returns {Promise<{src, desc, position, type, title, id, duration?}>}
   */
  async getChannelList () {
    let self = this
    this.params.channelList = await wrap(() => fetch(self.params.channelListUrl + '?' + new Date().valueOf())).then(res => res.json())
    return this.params.channelList
  },
  async getSessData (path = '/') {
    let self = this
    return wrap(() => fetch(self.params.sessData + '?' + new Date().valueOf()))
  },
  getLocalization () {
    return this.params.lang
  },
  setLocalization (locale) {
    console.info('localization change to ' + locale)
    localStorage.setItem('localization', locale)
    this.params.lang = locale
    Vue.$i18n = locale
  },
  getCustom () {
    return this.params.customText
  }
}
