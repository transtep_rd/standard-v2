import { recursive } from 'merge'
import axios from 'axios'

function toLogger (facility, msg, level, time = false) {
  let send = {
    facility,
    msg,
    level
  }

  if (time) {
    send = recursive(true, send, { time })
  }
  return axios.post('http://localhost/app/rest/logger.php', send)
    .then(res => res.data)
    .then(res => res.data.state === true)
    .catch(error => {
      console.warn(error)
      return false
    })
}

export default {
  async log (msg, facility = 'GUI') {
    msg = typeof msg === 'string' ? msg : JSON.stringify(msg)
    let state = await toLogger(facility, msg, 'log')
    return state
  },
  async advertisement (id, title) {
    let state = toLogger('ads-play', JSON.stringify({ id, title }), 'log')
    return state
  }
}
