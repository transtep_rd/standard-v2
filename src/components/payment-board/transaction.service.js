import ebus from '../../shared/ebus'
import { IVM_DEFAULT_TEXT } from '../../shared/default-text.js'

export function midHandler (mid, defaultMsg = '') {
  if (!mid) return false
  let result = {
    text: defaultMsg,
    isAlert: false,
    time: -1
  }
  let regex = /^\[([^\]]+)\]/
  if (mid in IVM_DEFAULT_TEXT) {
    /** @type {string} */
    let text = IVM_DEFAULT_TEXT[mid]
    let regStr = text.match(regex)
    result.text = text.replace(regStr[0], '')
    result.isAlert = regStr[1].startsWith('!')
    result.time = parseInt(regStr[1].replace(/(.*):/, ''))
  } else {
    if (/(error|fail|success)/.test(mid)) {
      result.time = 5
      result.isAlert = /(error|fail)/.test(mid)
    } else {
      return false
    }
  }
  console.log(result)
  return result
}
/**
 * 呼叫倒數跑條
 * @param {number} sec 時間
 * @param {Function} cb Function
 * @param {boolean} showTimeline 顯示時間軸
 */
export const showTimeline = (sec, cb, showTimeline = false) => ebus.emit('ipc:showTransactionTimeline', sec, cb, showTimeline)
export const hideTimeline = () => ebus.emit('ipc:hideTransactionTimeline')
