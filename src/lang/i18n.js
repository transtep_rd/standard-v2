import Vue from 'vue'
import VueI18n from 'vue-i18n'

import zhtw from './zhtw'
import enus from './en'
import myMM from './my-MM'

// 預設語言別
let loc = 'zh-tw'

/** @description LCIDString https://www.science.co.il/language/Locale-codes.php */
let messages = {
  'zh-tw': zhtw,
  'en-us': enus,
  'my-MM': myMM
}

Vue.use(VueI18n)

let i18n = new VueI18n({
  locale: loc,
  messages
})

export default i18n
