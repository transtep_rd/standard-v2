import i18n from '@/lang/i18n'
import TransactionReceive from '@/components/payment-board/transaction-receive-select'
import ebus from '@/shared/ebus'
import assert from 'assert'
import { createTest, destroyVM } from '../util'

describe('TransactionReceive', () => {
  let vm
  before(() => {
    vm = createTest(TransactionReceive, {i18n})
  })
  after(() => {
    vm.$destroy()
    destroyVM(vm)
  })
  it('should emit end', (done) => {
    vm.$once('end', () => done())
    vm.onClick(true)
  })
  it('should send ' + JSON.stringify({ e: 'invoice/input', arg: { 'invoice_receipt': true } }), (done) => {
    ebus.once('send', (data) => {
      let sendData = { e: 'invoice/input', arg: { 'invoice_receipt': true }, src: 'gui' }
      assert.equal(data.e, 'invoice/input')
      assert.deepEqual(data.arg, sendData.arg)
      done()
    })
    vm.onClick(true)
  })
  it('ebus should send ' + JSON.stringify({ e: 'invoice/cancel' }), (done) => {
    ebus.once('send', (data) => {
      assert.equal(data.e, 'invoice/cancel')
      assert.equal(data.src, 'gui')
      done()
    })
    vm.onClick(false)
  })
})
