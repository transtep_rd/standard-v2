import CancelSpinner from '@/components/payment-board/cancel-spinner'
import { createTest, destroyVM } from '../util'
import i18n from '@/lang/i18n'

describe('Cancel 組件', () => {
  let vm
  before(() => {
    vm = createTest(CancelSpinner, { propsData: { pause: false, initSec: 1 }, i18n })
  })
  it('should pause', function (done) {
    this.timeout(5000)
    vm.pause = true
    vm.$once('end', () => {
      done('should not got end')
    })
    setTimeout(done, 3100)
  })

  it('should emit end ', function (done) {
    vm.second = 4
    vm.pause = false
    this.timeout(6 * 1000)
    vm.$once('end', () => {
      done()
    })
  })
  it('destroy complete', () => {
    vm.$destroy()
    destroyVM(vm)
  })
})
