import CashContainer from '@/components/payment-board/cash-container'
import { createTest, destroyVM, nextTick } from '../util'
import ebus from '@/shared/ebus'
import i18n from '@/lang/i18n'
import assert from 'assert'

describe('CashContainer測試', () => {
  let vm
  before(() => {
    vm = createTest(CashContainer, {
      i18n,
      propsData: {
        price: 500
      }
    })
  })
  after(() => {
    vm.$destroy()
    destroyVM(vm)
  })
  it('payment/after_payment_begin 非 cash項目會關閉', () => {
    ebus.emit('payment/after_payment_begin', {
      e: 'payment/after_payment_begin',
      arg: {
        method: 'ipass'
      }
    })
    assert.deepEqual(vm.isShow, false)
  })
  it('payment/after_payment_begin cash項目 isShow為true', () => {
    ebus.emit('payment/after_payment_begin', {
      e: 'payment/after_payment_begin',
      arg: {
        method: 'cash',
        paid: 100
      }
    })
    assert.equal(vm.isShow, true)
  })
  it('payment/after_hint paid會更新資訊', async () => {
    ebus.emit('payment/after_hint', {
      e: 'payment/after_hint',
      arg: {
        paid: 100
      }
    })
    await nextTick()
    assert.equal(vm.paid, 100)
    assert.equal(vm.$el.querySelector('.cash-inserted-hint').innerText, vm.$t('PLEASE_INSERT_THE_CASH'))
  })
  it('payment/after_paid 會更新資訊', async () => {
    ebus.emit('payment/after_paid', {
      e: 'payment/after_paid',
      arg: {
        paid: 500
      }
    })
    await nextTick()
    assert.equal(vm.paid, 500)
    assert.equal(vm.$el.querySelector('.cash-inserted-hint').innerText, vm.$t('CASH_WILL_REFUND'))
  })
})
