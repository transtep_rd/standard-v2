import { equal, deepEqual } from 'assert'
import { midHandler, showTimeline, hideTimeline } from '@/components/payment-board/transaction.service'
import { createTest, destroyVM, nextTick } from '../util'
import Timeline from '@/components/payment-board/transaction-timeline'
import i18n from '@/lang/i18n'
const MAX_TIMEOUT = 4000
describe('TransactionService Test', () => {
  describe('mid測試', () => {
    it('mid 空值回傳false', () => {
      equal(midHandler(undefined), false)
    })
    it('mid 為processing回傳正確', () => {
      deepEqual(midHandler('processing'), { text: 'PROCESSING', isAlert: false, time: -1 })
    })
    it('mid帶有關鍵字fail/error/success的處理行為, 及添加預設字詞', () => {
      deepEqual(midHandler('sth_success', 'test'), {
        time: 5,
        isAlert: false,
        text: 'test'
      })
      deepEqual(midHandler('sth_failed', 'test'), {
        time: 5,
        isAlert: true,
        text: 'test'
      })
      deepEqual(midHandler('sth_error', 'test'), {
        time: 5,
        isAlert: true,
        text: 'test'
      })
    })
    it('mid非屬於fail/error/success回傳false', () => {
      equal(midHandler('qwertyasdfgzxcv'), false)
    })
  })
  describe('timeline測試', () => {
    let vm
    beforeEach(() => {
      vm = createTest(Timeline, { i18n })
    })
    afterEach(() => {
      vm.$destroy()
      destroyVM(vm)
    })
    it('timeline 應該顯示', async () => {
      showTimeline(-1, () => { }, true)
      await nextTick()
      equal(vm.isShow, true)
    })
    it('timeline結束執行callback function', (done) => {
      showTimeline(1, done, false)
    })
    it('timeline取消', function (done) {
      this.timeout(MAX_TIMEOUT)
      showTimeline(3, () => {
        done('oops!被執行了！')
      }, true)
      hideTimeline()
      setTimeout(() => { done() }, 2 * 1000)
    })
  })
})
