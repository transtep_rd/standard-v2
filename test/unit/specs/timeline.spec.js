import Timeline from '@/components/payment-board/transaction-timeline'
import { createTest, destroyVM, nextTick } from '../util'
import i18n from '@/lang/i18n'
import { showTimeline, hideTimeline } from '@/components/payment-board/transaction.service'
import assert from 'assert'
import Vue from 'vue'

describe('Timeline', () => {
  let vm
  before(() => {
    vm = createTest(Timeline, { i18n })
  })
  after(() => {
    vm.$destroy()
    destroyVM(vm)
  })
  it('Should execute end function after 2 sec', function (done) {
    this.timeout(3000)
    showTimeline(2, async () => {
      await nextTick()
      assert.equal(vm.isShow, false)
      done()
    }, true)
    Vue.nextTick(() => {
      assert.equal(vm.isShow, true)
    })
  })
  it('Should be closed', function (done) {
    this.timeout(3000)
    showTimeline(2, () => {
      done('這裡不應該被執行')
    })
    hideTimeline()
    Vue.nextTick(() => {
      assert.equal(vm.isShow, false)
      done()
    })
  })
  it('Should not be executed', function (done) {
    this.timeout(5000)
    showTimeline(-1, () => {
      done('這裡不應該被執行')
    })
    setTimeout(() => {
      done()
    }, 4000)
  })
  it('destroy', () => {
    vm.$destroy()
  })
})
