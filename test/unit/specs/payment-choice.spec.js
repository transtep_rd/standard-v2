import PaymentChoice from '@/components/payment-board/payment-choice'
import { createTest, destroyVM, nextTick } from '../util'
import i18n from '@/lang/i18n'
import assert from 'assert'
import ebus from '@/shared/ebus'

describe('交易面板 底下選項組件', () => {
  let vm
  describe('空值(Coverage用)', () => {
    before(() => {
      vm = createTest(PaymentChoice, {
        i18n
      })
    })
    after(() => {
      vm.$destroy()
      destroyVM(vm)
    })
    it('空陣列', () => {
      assert.deepEqual(vm.paymentMethodsHasIntroduction, [])
    })
  })
  describe('有現金', () => {
    before(() => {
      vm = createTest(PaymentChoice, {
        i18n,
        propsData: {
          paymentMethodsList: {
            cash: {
              price: 100
            },
            easycardedc: {
              price: 100
            },
            'isc_anycode': {
              price: 1000
            },
            '': {
              price: 50
            }
          }
        }
      })
    })
    after(() => {
      vm.$destroy()
      destroyVM(vm)
    })
    it('Should show two select view', async () => {
      await nextTick()
      let amount = vm.$el.querySelector('.no-cash').childElementCount
      assert.equal(vm.paymentMethodsHasIntroduction.length, 2)
      assert.equal(amount, 2)
    })
    it('觸發payment/input啟用支付', (done) => {
      ebus.once('send', (data) => {
        assert.equal(data.e, 'payment/input')
        done()
      })
      vm.paymentTrigger('easycardedc')
    })
    it('現金支付透過ebus開始進行', async () => {
      ebus.emit('payment/after_payment_begin', {
        e: 'payment/after_payment_begin',
        arg: {
          method: 'cash',
          paid: 10
        }
      })
      await nextTick()
      assert.equal(vm.selectCash, true)
      assert.equal(vm.afterPaymentBegin, true)
    })
  })
  describe('無現金，無QRCode', () => {
    before(() => {
      vm = createTest(PaymentChoice, {
        i18n,
        propsData: {
          paymentMethodsList: {
            easycardedc: {
              price: 100
            }
          }
        }
      })
    })
    after(() => {
      vm.$destroy()
      destroyVM(vm)
    })
    it('Should show one select view', async () => {
      await nextTick()
      let amount = vm.$el.querySelector('.no-cash').childElementCount
      assert.equal(vm.phonePayCategories('test'), undefined)
      assert.equal(vm.paymentMethodsHasIntroduction.length, 1)
      assert.equal(amount, 1)
    })
    it('悠遊卡開始進行', () => {
      vm.onPaymentBegin({
        e: 'payment/after_payment_begin',
        arg: {
          method: 'easycardedc'
        }
      })
      assert.equal(vm.selectCash, false)
      assert.equal(vm.afterPaymentBegin, true)
    })
    it('觸發payment/input啟用支付', (done) => {
      ebus.once('send', (data) => {
        assert.equal(data.e, 'payment/input')
        done()
      })
      vm.paymentTrigger('easycardedc')
    })
  })
})
