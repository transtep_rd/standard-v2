import TransactionInteractive from '@/components/payment-board/transaction-interactive'
import { createTest, destroyVM, nextTick } from '../util'
import i18n from '@/lang/i18n'
import { equal, deepEqual } from 'assert'
import ebus from '@/shared/ebus'

const TIMEOUT = 2000
const NORMAL_TIMEOUT = 5000

describe('交易互動選項操作', () => {
  let vm
  beforeEach(() => {
    vm = createTest(TransactionInteractive, { i18n })
  })
  afterEach(() => {
    vm.$destroy()
    destroyVM(vm)
  })
  it('派送空值且非afterPaymentBegin 關閉提示框', async () => {
    vm.onPayment({
      e: 'payment/after_hint',
      arg: {
        mid: 'processing'
      }
    })
    vm.onPayment({
      e: 'payment/after_hint',
      arg: {}
    })
  })
  describe('Payment相關', () => {
    it('交易訊息提示: 悠遊卡', async () => {
      vm.onPayment({
        e: 'payment/after_input',
        arg: {
          mid: 'payment_wait_easycardedc',
          msg: '請靠卡',
          method: 'easycardedc'
        }
      })
      await nextTick()
      equal(vm.isAlert, false)
      equal(vm.topic, 'PAYMENT_WAIT_EASYCARDEDC')
    })
    it('afterPaymentBegin觸發之前不會end', function (done) {
      this.timeout(TIMEOUT + 1000)
      ebus.emit('payment/after_hint', {
        e: 'payment/after_hint',
        arg: {
          mid: 'processing',
          msg: 'processing'
        }
      })
      vm.$once('end', () => {
        done('should not be called')
      })
      setTimeout(() => {
        done()
      }, TIMEOUT)
    })
    it('afterPaymentBegin 後會 emit end', function (done) {
      this.timeout(5500)
      ebus.emit('payment/after_payment_begin', {
        e: 'payment/after_payment_begin',
        arg: {
          mid: 'processing'
        }
      })
      nextTick()
        .then(() => {
          ebus.emit('payment/after_failed', {
            e: 'payment/after_failed',
            arg: {
              mid: 'payment_timeout'
            }
          })
        })
      vm.$once('end', () => {
        done()
      })
    })
    it('topicArg帶入順利', async function () {
      this.timeout(NORMAL_TIMEOUT + 1000)
      vm.onPayment({
        e: 'payment/after_payment_begin',
        arg: {
          mid: 'processing'
        }
      })
      vm.onPayment({
        e: 'payment/after_hint',
        arg: {
          mid: 'easycardedc_failed',
          msg: 'test'
        }
      })
      await nextTick()
      deepEqual(vm.topicArg, { 'var': 'test' })
    })
    it('processing後讀取得到交易準備完成資訊，自行關掉', async () => {
      vm.onPayment({
        e: 'payment/after_hint',
        arg: {
          mid: 'processing',
          msg: '處理中'
        }
      })
      equal(vm.isShow, true)
      vm.onPayment({
        e: 'payment/after_hint',
        arg: {
          payment_method: {
            easycardedc: {
              price: 100
            }
          }
        }
      })
      equal(vm.isShow, false)
    })
    it('payment/after_paid 蒐集資料', async () => {
      ebus.emit('payment/after_paid', {
        e: 'payment/paid',
        arg: {
          data: 'test'
        }
      })
      await nextTick()
      equal(vm.paymentDetail, 'test')
    })
    it('payment/after_payment_begin 尚未收到，timeline到時會將互動框關閉', function (done) {
      this.timeout(TIMEOUT + 4000)
      vm.onPayment({
        e: 'payment/after_hint',
        arg: {
          mid: 'printer_disabled'
        }
      })
      setTimeout(() => {
        equal(vm.isShow, false)
        done()
      }, 5500)
    })
  })
  it('出貨顯示成功後，觸發end', function (done) {
    this.timeout(6000)
    vm.$once('end', () => {
      done()
    })
    vm.onDispense({
      e: 'dispense/after_prod_dispensed',
      arg: {
        mid: 'dispense_ok'
      }
    })
  })
  describe('明細相關', () => {
    it('統編輸入', () => {
      vm.onInvoice({
        e: 'invoice/after_hint',
        arg: {
          invoice_unino: false,
          mid: 'q_input_unino'
        }
      })
      equal(vm.activeMod, 'unino')
    })
    it('明細輸入', () => {
      vm.onInvoice({
        e: 'invoice/after_hint',
        arg: {
          invoice_receipt: false,
          mid: 'q_print_receipt'
        }
      })
      equal(vm.activeMod, 'invoice')
    })
    it('非統編及明細', () => {
      vm.onInvoice({
        e: 'invoice/after_success',
        arg: {
          mid: 'receipt_print_done'
        }
      })
    })
    it('列印中', () => {
      vm.receiveEnded()
      equal(vm.topic, 'INVOICE_PRINTING')
      vm.uninoEnded()
      equal(vm.topic, 'INVOICE_PRINTING')
    })
  })
})
