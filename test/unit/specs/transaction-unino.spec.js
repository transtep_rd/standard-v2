import i18n from '@/lang/i18n'
import TransactionUnino from '@/components/payment-board/transaction-unino-input'
import ebus from '@/shared/ebus'
import { createTest, destroyVM, nextTick } from '../util'
import assert from 'assert'
import Timeline from '@/components/payment-board/transaction-timeline'
import { hideTimeline } from '@/components/payment-board/transaction.service'
const UNINO_TIME = 1
let timeline = createTest(Timeline, { i18n })
after(() => {
  timeline.$destroy()
  destroyVM(timeline)
})

describe('TransactionUnino', () => {
  let vm
  describe('正常流程', () => {
    let onEnd = false
    before(() => {
      vm = createTest(TransactionUnino, { i18n })
    })
    after(() => {
      vm.$destroy()
      destroyVM(vm)
    })
    it('should show question', async () => {
      await nextTick()
      assert.notEqual(vm.$el.querySelector('.question'), null)
    })
    it('於詢問視窗點選確認切換到輸入畫面', async () => {
      await nextTick()
      vm.$el.querySelectorAll('.question>button')[1].click()
      await nextTick()
      assert.equal(vm.is, 'input')
    })
    it('輸入畫面無法點選確認', async () => {
      assert.equal(vm.$refs['confirm-btn'].disabled, true)
    })
    it('輸入12345678 顯示錯誤信息', async () => {
      [1, 2, 3, 4, 5, 6, 7, 8].forEach(num => {
        vm.numpadOnClick(num)
      })
      await nextTick()
      assert.equal(vm.uninoValidation().alert, true)
      assert.equal(vm.uninoValidation().text, `統一編號：12345678 錯誤!`)
    })
    it('點選確認無法進行到下一階段', () => {
      vm.numpadOnClick('done')
      assert.notDeepEqual(vm.is, 'result')
    })
    it('Delete should be work', async () => {
      vm.numpadOnClick('delete')
      await nextTick()
      assert.equal(vm.$el.querySelector('input').value, '1234567')
      '1234567'.split('').forEach(ele => {
        vm.numpadOnClick('delete')
      })
    })
    it('53541976 should work', async () => {
      '53541976'.split('').forEach(e => {
        vm.numpadOnClick(e)
      })
      await nextTick()
      assert.equal(vm.$refs['confirm-btn'].disabled, false)
    })
    it('統一編號判斷應為成功', () => {
      assert.equal(vm.uninoValidation().alert, false)
    })
    it('最多輸入8個數字', async () => {
      vm.numpadOnClick('3')
      assert.equal(vm.unino.length, 8)
    })
    it('alert should not show after input correct unino', () => {
      assert.equal(vm.$el.querySelector('.alert'), null)
      assert.equal(vm.isWarning, false)
    })
    it('輸入後確認', async () => {
      vm.numpadOnClick('done')
      await nextTick()
      assert.equal(vm.is, 'result')
    })
    it('重複確認後送出', done => {
      vm.$once('end', () => {
        onEnd = true
      })
      ebus.once('send', (data) => {
        assert.equal(data.e, 'invoice/input')
        assert.equal(data.arg.invoice_unino, '53541976')
        done()
      })
      vm.result(true)
    })
    it('components already emit end', () => {
      assert.equal(onEnd, true)
    })
  })
  describe('直接取消', () => {
    before(() => {
      vm = createTest(TransactionUnino, { i18n })
    })
    after(() => {
      vm.$destroy()
      destroyVM(vm)
    })
    it('直接點取消', (done) => {
      ebus.once('send', data => {
        assert.equal(data.e, 'invoice/input')
        assert.equal(data.arg.invoice_unino, false)
        done()
      })
      vm.question(false)
    })
  })
  describe(`逾時取消（${UNINO_TIME}秒）`, () => {
    before(() => {
      vm = createTest(TransactionUnino, { i18n })
      vm.uninoTime = UNINO_TIME
    })
    after(() => {
      vm.$destroy()
      destroyVM(vm)
      hideTimeline()
    })
    it('should emit end and return cancel', function (done) {
      ebus.once('send', data => {
        assert.equal(data.e, 'invoice/input')
        assert.equal(data.arg.invoice_unino, false)
        done()
      })
    })
  })
  describe(`輸入畫面後取消，逾時退出（${UNINO_TIME}秒）`, () => {
    before(() => {
      vm = createTest(TransactionUnino, { i18n })
      vm.uninoTime = UNINO_TIME
    })
    after(() => {
      vm.$destroy()
      destroyVM(vm)
    })
    it('點選進入統一編號輸入', () => {
      vm.question(true)
      assert.equal(vm.is, 'input')
    })
    it('點選取消後1秒自動發出取消', function (done) {
      this.timeout(UNINO_TIME * 2 * 1000)
      ebus.once('send', data => {
        assert.equal(data.e, 'invoice/input')
        assert.equal(data.arg.invoice_unino, false)
        done()
      })
      vm.numpadOnClick('cancel')
    })
  })
  describe(`完成輸入timeout自動送出（${UNINO_TIME}秒）`, () => {
    before(() => {
      vm = createTest(TransactionUnino, { i18n })
      vm.uninoTime = UNINO_TIME
    })
    after(() => {
      vm.$destroy()
      destroyVM(vm)
    })
    it('進入input', () => {
      vm.question(true)
      assert.equal(vm.is, 'input')
    })
    it('輸入正確統一編號', () => {
      '53541976'.split('').forEach(e => {
        vm.numpadOnClick(e)
      })
      assert.equal(vm.unino, '53541976')
    })
    it('模擬返回至輸入做確認', () => {
      vm.numpadOnClick('done')
      vm.result(false)
      assert.equal(vm.is, 'input')
      assert.equal(vm.unino, '53541976')
    })
    it('確認後一秒送出', done => {
      ebus.once('send', data => {
        assert.equal(data.e, 'invoice/input')
        assert.equal(data.arg.invoice_unino, '53541976')
        done()
      })
      vm.numpadOnClick('done')
    })
  })
})
